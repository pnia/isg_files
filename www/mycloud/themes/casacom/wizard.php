<div id="firstrunwizard">

<a id="closeWizard" class="close">
	<img class="svg" src="<?php print_unescaped(OCP\Util::imagePath('core', 'actions/close.svg')); ?>">
</a>
<h1><?php p($l->t('Willkommen zu %s', array($theme->getTitle()))); ?></h1>
<?php if (OC_Util::getEditionString() === ''): ?>
<p><?php p($l->t('.'));?></p>
<?php else: ?>
<p><?php p($theme->getSlogan()); ?></p>
<?php endif; ?>

<p class="footnote">
© 2015-2016 <a href="http://www.casacom.ch" target="_blank">casacom solutions AG</a>
</p>

</div>
